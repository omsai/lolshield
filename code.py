"""Port of Arduino Lots of LEDs (LoL) shield to CircuitPython.

The LoLshield is a 9x14 array of LEDs that mounts on Arduino compatible boards.
The original Arduino library is at: https://github.com/jprodgers/LoLshield/ and
the hardware is described at http://jimmieprodgers.com/kits/lolshield/

This CircuitPython library allows you to access individual LEDs and provides
higher level abstractions like 12-hour clock digits; there are not enough LEDs
for 24-hour clock display.

"""
import board
from digitalio import DigitalInOut, Direction


def init_leds():
    """Return pins to access Charlieplexed LEDs."""
    # Hard code the sheild's control pins D2-D13.
    pins = [DigitalInOut(getattr(board, pin))
            for pin in ['D{}'.format(i) for i in range(2, 14)]]
    # Change all pins into inputs to emulate open-circuit to turn off board.
    for pin in pins:
        pin.direction = Direction.INPUT
    return pins


def L(high, low):               # pylint: disable=invalid-name
    """Return LED high-low pair from named digital pins."""
    return (PINS[high - 2], PINS[low - 2],)


def led_on(led):
    """Turn on a single LED."""
    if led is None:
        return
    led[0].direction = Direction.OUTPUT
    led[0].value = True
    led[1].direction = Direction.OUTPUT
    led[1].value = False


def led_off(led):
    """Turn off a single LED."""
    if led is None:
        return
    led[0].direction = Direction.INPUT
    led[1].direction = Direction.INPUT


PINS = init_leds()
# Row major ordering of LEDs from
# https://github.com/jprodgers/LoLshield/blob/master/Charliplexing.cpp
#
# pylint: disable=bad-whitespace
# flake8: noqa
LEDS = [
    L(13,  5), L(13, 6), L(13,  7), L(13, 8), L(13,  9), L(13, 10), L(13, 11),
    L(13, 12), L(13, 4), L( 4, 13), L(13, 3), L( 3, 13), L(13,  2), L( 2, 13),
    L(12,  5), L(12, 6), L(12,  7), L(12, 8), L(12,  9), L(12, 10), L(12, 11),
    L(12, 13), L(12, 4), L( 4, 12), L(12, 3), L( 3, 12), L(12,  2), L( 2, 12),
    L(11,  5), L(11, 6), L(11,  7), L(11, 8), L(11,  9), L(11, 10), L(11, 12),
    L(11, 13), L(11, 4), L( 4, 11), L(11, 3), L( 3, 11), L(11,  2), L( 2, 11),
    L(10,  5), L(10, 6), L(10,  7), L(10, 8), L(10,  9), L(10, 11), L(10, 12),
    L(10, 13), L(10, 4), L( 4, 10), L(10, 3), L( 3, 10), L(10,  2), L( 2, 10),
    L( 9,  5), L( 9, 6), L( 9,  7), L( 9, 8), L( 9, 10), L( 9, 11), L( 9, 12),
    L( 9, 13), L( 9, 4), L( 4,  9), L( 9, 3), L( 3,  9), L( 9,  2), L( 2,  9),
    L( 8,  5), L( 8, 6), L( 8,  7), L( 8, 9), L( 8, 10), L( 8, 11), L( 8, 12),
    L( 8, 13), L( 8, 4), L( 4,  8), L( 8, 3), L( 3,  8), L( 8,  2), L( 2,  8),
    L( 7,  5), L( 7, 6), L( 7,  8), L( 7, 9), L( 7, 10), L( 7, 11), L( 7, 12),
    L( 7, 13), L( 7, 4), L( 4,  7), L( 7, 3), L( 3,  7), L( 7,  2), L( 2,  7),
    L( 6,  5), L( 6, 7), L( 6,  8), L( 6, 9), L( 6, 10), L( 6, 11), L( 6, 12),
    L( 6, 13), L( 6, 4), L( 4,  6), L( 6, 3), L( 3,  6), L( 6,  2), L( 2,  6),
    L( 5,  6), L( 5, 7), L( 5,  8), L( 5, 9), L( 5, 10), L( 5, 11), L( 5, 12),
    L( 5, 13), L( 5, 4), L( 4,  5), L( 5, 3), L( 3,  5), L( 5,  2), L( 2,  5),
]


def demo_each_led():
    """Cycle through each LED."""
    import time                 # pylint: disable=import-outside-toplevel
    for led in LEDS:
        led_on(led)
        time.sleep(0.01)
        led_off(led)
        time.sleep(0.04)


# Clock digits.
#
# Seven segment display
# https://en.wikipedia.org/wiki/Seven-segment_display#Hexadecimal
A = [29, 30, 31]
B = [31, 45, 59]
C = [59, 73, 87]
D = [85, 86, 87]
E = [57, 71, 85]
F = [29, 43, 57]
G = [57, 58, 59]
DIGITS = [
    set(A + B + C + D + E + F),
    set(B + C),
    set(A + B + G + E + D),
    set(A + B + G + C + D),
    set(F + G + B + C),
    set(A + F + G + C + D),
    set(A + F + G + C + D + E),
    set(A + B + C),
    set(A + B + C + D + E + F + G),
    set(A + B + C + F + G),
]
# Hours and minutes digit offsets.
CLOCK_DIGIT_OFFSETS = [-2, 2, 6, 10]


def demo_hour_digits():
    """Cycle the hour units digit though all possible numbers."""
    offset = CLOCK_DIGIT_OFFSETS[1]
    for digit in DIGITS:
        counter = 0
        while counter < 4e3 / len(digit):
            for i in digit:
                led_on(LEDS[i + offset])
                led_off(LEDS[i + offset])
            counter += 1


def demo_clock(hours, minutes):
    """Print clock time."""
    assert 0 <= hours <= 12
    assert 0 <= minutes <= 59
    # Convert time into individual digits.
    time = [
        hours // 10,
        hours % 10,
        minutes // 10,
        minutes % 10,
    ]
    # Strip any leading 0 from hours position.
    offsets = CLOCK_DIGIT_OFFSETS
    if time[0] == 0:
        time = time[1:]
        offsets = offsets[1:]
    # Gather LED set of digits into a frame for rendering.
    frame = {i + offset
             for digit, offset in zip(time, offsets)
             for i in DIGITS[digit]}
    # Render.
    counter = 0
    while counter < 1e4 / len(frame):
        for i in frame:
            led_on(LEDS[i])
            led_off(LEDS[i])
        counter += 1


if __name__ == '__main__':
    # Disable the faulty LED on my board.
    LEDS[14 * 4] = None
    while True:
        demo_clock(12, 59)
        demo_clock(0, 0)
        demo_clock(10, 10)
        demo_hour_digits()
        demo_each_led()
